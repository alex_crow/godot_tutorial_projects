extends Area2D

signal hit

# How fast the player will move (pixels/sec)
@export var speed = 400
# Size of the game window
var screen_size

# Called when the node enters the scene tree for the first time.
func _ready():
  screen_size = get_viewport_rect().size

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
  # The player's movement vector
  var velocity = Vector2.ZERO
  if Input.is_action_pressed("move_right"):
    velocity.x += 1
  if Input.is_action_pressed("move_left"):
    velocity.x -= 1
  if Input.is_action_pressed("move_down"):
    velocity.y += 1
  if Input.is_action_pressed("move_up"):
    velocity.y -= 1

  if velocity.length() > 0:
    velocity = velocity.normalized() * speed
    get_node("AnimatedSprite2D").play()
  else:
    get_node("AnimatedSprite2D").stop()
    
  position += velocity * delta
  position = position.clamp(Vector2.ZERO, screen_size)
  
  if velocity.x != 0:
    get_node("AnimatedSprite2D").animation = "walk"
    get_node("AnimatedSprite2D").flip_v    = false
    if velocity.x < 0:
      get_node("AnimatedSprite2D").flip_h = true
    else:
      get_node("AnimatedSprite2D").flip_h = false
  elif velocity.y != 0:
    get_node("AnimatedSprite2D").animation = "up"
    if velocity.y > 0:
      get_node("AnimatedSprite2D").flip_v = true
    else:
      get_node("AnimatedSprite2D").flip_v = false
      
func _on_body_entered(body):
  # Player disappears after being hit
  hide()
  hit.emit()
  # Must be deferred as we can't change physics properties on a physics callback
  get_node("CollisionShape2D").set_deferred("disabled", true)
  
func start(pos):
  position = pos
  show()
  get_node("CollisionShape2D").disabled = false
