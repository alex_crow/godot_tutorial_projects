extends Node2D

@onready var player_spawn_position : Node = $player_spawn_position
@onready var laser_container       : Node = $laser_container

var player : Node = null

func _ready() -> void:
  player = get_tree().get_first_node_in_group("player")
  assert(player != null)
  player.global_position = player_spawn_position.global_position
  player.laser_shot.connect(_on_player_laser_shot)

func _process(_delta: float) -> void:
  if Input.is_action_just_pressed("quit"):
    get_tree().quit()
  elif Input.is_action_just_pressed("reset"):
    get_tree().reload_current_scene()

func _on_player_laser_shot(laser_scene : Resource,
                           location    : Vector2) -> void:
  var laser : Node      = laser_scene.instantiate()
  laser.global_position = location
  laser_container.add_child(laser)
