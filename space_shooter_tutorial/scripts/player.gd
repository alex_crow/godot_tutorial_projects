extends CharacterBody2D

signal laser_shot(laser_scene, location)

@export var speed        : float = 300.0
@export var rate_of_fire : float = 0.25

@onready var muzzle : Node = $muzzle

var laser_scene    : Resource = preload("res://scenes/laser.tscn")
var shoot_cooldown : bool     = false

func _process(_delta : float) -> void:
  if Input.is_action_pressed("shoot"):
    if not shoot_cooldown:
      shoot_cooldown = true
      shoot()
      await get_tree().create_timer(rate_of_fire).timeout
      shoot_cooldown = false

func _physics_process(_delta : float) -> void:
  var direction : Vector2 = (
    Vector2(
      Input
        .get_vector(
          "move_left",
          "move_right",
          "move_up",
          "move_down"
        )
    )
  )
  velocity = direction * speed
  move_and_slide()

func shoot() -> void:
  laser_shot.emit(laser_scene, muzzle.global_position)
